package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.endpoint.Session;

public class TaskListShowCommand extends AbstractCommand {
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tascks";
    }

    @Override
    public void execute() {
        Session session = serviceLocator.getSession();
        serviceLocator.getTaskEndpoint().findAllTask(session).stream().forEach(o -> System.out.println(o.getName()));
        System.out.println("[OK]");
    }
}
