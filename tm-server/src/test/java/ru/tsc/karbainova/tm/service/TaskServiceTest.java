package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.repository.TaskRepository;

import java.util.List;


public class TaskServiceTest {
    @Nullable
    private TaskService taskService;
    @Nullable
    private Task task;
    private String userLogin = "test";

    @Before
    public void before() {
        taskService = new TaskService(new TaskRepository());
        taskService.add(userLogin, new Task("Task"));
        task = taskService.findByIndex(userLogin, 0);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NonNull final Task projectById = taskService.findById(task.getUserId(), task.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void findAll() {
        @NonNull final List<Task> projects = taskService.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        @NonNull final List<Task> projects = taskService.findAll(userLogin);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByErrorUserId() {
        @NonNull final List<Task> projects = taskService.findAll("ertyut");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findAllByName() {
        @NonNull final Task projects = taskService.findByName(userLogin, task.getName());
        Assert.assertNotNull(projects);
    }

    @Test
    public void findAllByErrorName() {
        @NonNull final Task projects = taskService.findByName(userLogin, "sdf");
        Assert.assertNull(projects);
    }

    @Test
    public void removeById() {
        taskService.remove(userLogin, task);
        Assert.assertNull(taskService.findById(userLogin, task.getId()));
    }

    @Test
    public void removeByErrorUserId() {
        @NonNull final Task projects = taskService.removeById("sd", task.getId());
        Assert.assertNull(projects);
    }
}
