package ru.tsc.karbainova.tm.command.data;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import javax.xml.bind.*;

import java.io.File;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "xml-load-jaxb";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Xml load Jax-B";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final File file = new File(FILE_JAXB_XML);
        @NonNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NonNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NonNull final Domain domain = (Domain) unmarshaller.unmarshal(file);

        setDomain(domain);
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
