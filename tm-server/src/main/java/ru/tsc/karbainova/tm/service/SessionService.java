package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.repository.ISessionRepository;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.ISessionService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.AccessDeniedException;
import ru.tsc.karbainova.tm.exception.AccessForbiddenException;
import ru.tsc.karbainova.tm.exception.empty.*;
import ru.tsc.karbainova.tm.model.Session;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.util.HashUtil;

import java.util.List;
import java.util.stream.Collectors;

public class SessionService extends AbstractService<Session> implements ISessionService {

    private final ServiceLocator serviceLocator;
    private final IPropertyService propertyService;
    private final ISessionRepository repository;

    public SessionService(
            final ISessionRepository sessionRepository,
            final ServiceLocator serviceLocator,
            final IPropertyService propertyService
    ) {
        super(sessionRepository);
        this.repository = sessionRepository;
        this.serviceLocator = serviceLocator;
        this.propertyService = propertyService;
    }

    @Override
    public boolean checkDataAccess(String login, String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new EmptyUserNotFoundException();
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public Session open(String login, String password) {
        final boolean check = checkDataAccess(login, password);
//        if (!check) throw new EmptyLoginOrPasswordException();
        if (check) throw new EmptyLoginOrPasswordException();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new EmptyLoginException();

        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        repository.add(session);
        return sign(session);
    }

    @Override
    public Session sign(Session session) {
        if (session == null) throw new EmptySessionNlException();
        session.setSignature(null);
        final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    public List<Session> getListSessionByUserId(String userId) {
        return findAll()
                .stream()
                .filter(s -> s.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public void validate(Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NonNull final String signatureSource = session.getSignature();
        @NonNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!repository.exists(session.getId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(Session session, Role role) {
        if (role == null) throw new AccessForbiddenException();
        validate(session);
        final String userId = session.getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessForbiddenException();
        if (user.getRole() == null) throw new AccessForbiddenException();
        if (!role.equals(user.getRole())) throw new AccessForbiddenException();
    }

    @Override
    public void close(Session session) {
        repository.remove(session);
    }

    @Override
    public void closeAll(Session session) {
        validate(session);
        List<Session> sessions = findAll().stream().filter(s -> s.getUserId().equals(session.getUserId())).collect(Collectors.toList());
        sessions.forEach(this::close);
    }

    @Override
    public void signOutByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return;
        List<Session> sessions = findAll().stream().filter(s -> s.getUserId().equals(userId)).collect(Collectors.toList());
        sessions.forEach(this::close);
    }

//    @Override
//    public void signOutByLogin(String login) {
//        if (login == null || login.isEmpty()) return;
//        final User user = serviceLocator.getUserService().findByLogin(login);
//        if (user == null) return;
//        final String userId = user.getId();
//        repository.removeByUserId(userId);
//    }
//

}
