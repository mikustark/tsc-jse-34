package ru.tsc.karbainova.tm.command.data;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataBase64LoadCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "data-load-base64";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load base64 data";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final byte[] base64Data = Files.readAllBytes(Paths.get(FILE_BINARY64));
        @NonNull final byte[] decodeData = Base64.getDecoder().decode(base64Data);
        @NonNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodeData);
        @NonNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NonNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
