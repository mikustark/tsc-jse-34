package ru.tsc.karbainova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadCommand extends AbstractDataCommand {

    public static String BACKUP_LOAD = "backup-load";

    @Override
    public String name() {
        return BACKUP_LOAD;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load backup";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull File file = new File(BACKUP_XML);
        if (!file.exists()) return;
        @NonNull final String xml = new String(Files.readAllBytes(Paths.get(BACKUP_XML)));
        @NonNull final ObjectMapper objectMapper = new XmlMapper();
        @NonNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[0];
    }
}
